## Installering
`npm install`

Dette prosjektet er skrevet i C# med Entity Framework i backend og React som frontend i browser. 
Testet med Postman og Firefox.

## Bruk
Start prosjektet via Visual Studio IIS Express eller lignende. Kalenderen vil åpnes i din standard nettleser.

For å legge til hendelser, kan du klikke på en dato. Dette vil åpne et lite vindu hvor du kan skrive inn beskrivelse av hendelsen. Hendelser lagt til når kalenderen viser måneder vil legges til som heldagshendelser. For å legge til en hendelse med spesifikk start og slutt tid, trykk på Uke eller Dag oppe i høyre hjørne, finn ønsket starttid, og klikk og dra musen ned til ønsket endetid. Legg til beskrivelse som tidligere og trykk OK.

For å slette en hendelse, trykk på hendelsen du vil slette og klikk OK i vinduet som dukker opp.

## Testing
Jeg valgte å bruke Postman for å teste backend. Vedlagt er 5 Postman tester, hvorav 2 er ment å feile (BadRequest og NotFound). Testene er satt til å bruke port 44373 som standard, men kan endres til å bruke en annen ved å endre URL-en i Postman. 

## Begrunnelse av designvalg
Jeg valgte å lage kalenderen med frontend basert på React ettersom jeg har jobbet mye med det de siste 3 ukene i case-perioden som inngår i Experis Academy's kurs, og synes det er relativt lett å jobbe med. For å gjøre det lett og ikke bruke for mye tid på frontend, brukte jeg React pakken big-calendar, slik at jeg kunne fokusere på backend og koblingen til frontend.

Jeg vurderte også å bruke et xUnit prosjekt for å teste backend, men ettersom backend hovedsaklig bruker Entity Framework, følte jeg at Postman testene holdt. Kalenderpakken jeg brukte i frontend hindrer uansett den gjennomsnittlige brukeren i å gjøre som kunne fått programmet til å kræsje (f.eks legge til hendelser på ugyldig dato), og ettersom kun det auto-genererte EventID-et er den eneste variabelen som brukes i queries, skal SQL injection ikke være mulig.