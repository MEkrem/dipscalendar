﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIPSCalendar.Models
{
  public class Event
  {
    public int EventID { get; set; }
    public string EventDesc { get; set; }
    public DateTime Start { get; set; }
    public DateTime End { get; set; }
    public bool AllDay { get; set; }
  }
}
