﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DIPSCalendar.Models
{
    public class EventContext : DbContext
    {
        public DbSet<Event> Events { get; set; }

        public EventContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var now = DateTime.Now;
            var later = DateTime.Now.AddHours(2);
            modelBuilder.Entity<Event>()
                .HasData(
                    new Event
                    {
                        EventID = 1,
                        EventDesc = "Eat pizza, raid BWL with SO",
                        Start = new DateTime(2020, 2, 14, 17, 00, 0),
                        End = new DateTime(2020, 2, 14, 23, 00, 0),
                        AllDay = false
                    },
                    new Event
                    {
                        EventID = 2,
                        EventDesc = "Dance like nobody's watching",
                        Start = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, now.Kind),
                        End = new DateTime(now.Year, now.Month, now.Day, later.Hour, now.Minute, now.Second, now.Kind),
                        AllDay = false
                    },
                    new Event
                    {
                        EventID = 3,
                        EventDesc = "Stare longingly out the window at the birds and squirrels frolicking in unquarantined freedom",
                        Start = new DateTime(2020, 3, 30, 12, 00, 0),
                        End = new DateTime(2020, 3, 30, 15, 00, 0),
                        AllDay = false
                    },
                    new Event
                    {
                        EventID = 4,
                        EventDesc = "Eat candy, watch Hot Fuzz",
                        Start = new DateTime(2020, 4, 4, 20, 0, 0),
                        End = new DateTime(2020, 4, 4, 22, 0, 0),
                        AllDay = false
                    },
                    new Event
                    {
                        EventID = 5,
                        EventDesc = "Doggysit for Rico",
                        Start = new DateTime(2020, 4, 15, 12, 30, 0),
                        End = new DateTime(2020, 4, 15, 17, 30, 0),
                        AllDay = false
                    },
                    new Event
                    {
                        EventID = 6,
                        EventDesc = "Browse puppies for sale",
                        Start = new DateTime(2020, 4, 15, 18, 00, 0),
                        End = new DateTime(2020, 4, 15, 19, 30, 0),
                        AllDay = false
                    },
                    new Event
                    {
                        EventID = 7,
                        EventDesc = "Pretend to sleep, wait for Santa",
                        Start = new DateTime(2020, 12, 24, 22, 00, 0),
                        End = new DateTime(2020, 12, 24, 23, 58, 0),
                        AllDay = false
                    },
                    new Event
                    {
                        EventID = 8,
                        EventDesc = "(Hopefully) play WoW Classic: TBC",
                        Start = new DateTime(2021, 1, 18, 23, 59, 0),
                        End = new DateTime(2021, 1, 19, 23, 59, 0),
                        AllDay = true
                    }
                );
        }
    }
}
