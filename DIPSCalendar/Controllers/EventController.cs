﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DIPSCalendar.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace DIPSCalendar.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly EventContext _context;
        
        public EventController(EventContext context)
        {
            _context = context;
        }


        //Error message curtesy of https://entityframework.net/how-to-see-ef-errors
        private bool SaveContext()
        {
            bool success = false;
            try
            {
                _context.SaveChanges();
                success = true;
                return success;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in the state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }

                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                            ve.PropertyName,
                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                            ve.ErrorMessage);
                    }
                }
                return success;
            }
        }

        [HttpGet]
        public IEnumerable<Event> GetEvents()
        {
            return _context.Events;
        }

        [HttpPost]
        public ActionResult<Event> AddEvent(Event newEvent)
        {
            _context.Events.Add(newEvent);
            if (SaveContext())
            {
                return CreatedAtAction("AddEvent",
                    new Event
                    {
                        EventID = newEvent.EventID,
                        EventDesc = newEvent.EventDesc,
                        Start = newEvent.Start,
                        End = newEvent.End,
                        AllDay = newEvent.AllDay
                    });
            }
            return BadRequest();
        }

        [HttpDelete("{eventID}")]
        public ActionResult<Event> DeleteEvent(int eventID)
        {
            Event eventToDelete = _context.Events.Find(eventID);
            if (eventToDelete != null)
            {
                _context.Events.Remove(eventToDelete);
                if (SaveContext())
                {
                    return eventToDelete;
                }
                return BadRequest();
            }
            return NotFound();
        }

    }
}