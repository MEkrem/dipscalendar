﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DIPSCalendar.Migrations
{
    public partial class RemovemsfromNow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 3, 27, 23, 51, 15, 0, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 3, 27, 23, 43, 22, 0, DateTimeKind.Local));
        }
    }
}
