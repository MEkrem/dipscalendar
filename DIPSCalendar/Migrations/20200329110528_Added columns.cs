﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DIPSCalendar.Migrations
{
    public partial class Addedcolumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "Events");

            migrationBuilder.AddColumn<bool>(
                name: "AllDay",
                table: "Events",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "End",
                table: "Events",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "Start",
                table: "Events",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 1,
                columns: new[] { "End", "Start" },
                values: new object[] { new DateTime(2020, 2, 14, 23, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 2, 14, 17, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 2,
                columns: new[] { "End", "Start" },
                values: new object[] { new DateTime(2020, 3, 29, 15, 5, 27, 0, DateTimeKind.Local), new DateTime(2020, 3, 29, 13, 5, 27, 0, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 3,
                columns: new[] { "End", "Start" },
                values: new object[] { new DateTime(2020, 3, 30, 15, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 3, 30, 12, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 4,
                columns: new[] { "End", "Start" },
                values: new object[] { new DateTime(2020, 4, 4, 22, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 4, 4, 20, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 5,
                columns: new[] { "End", "Start" },
                values: new object[] { new DateTime(2020, 4, 15, 17, 30, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 4, 15, 12, 30, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 6,
                columns: new[] { "End", "Start" },
                values: new object[] { new DateTime(2020, 4, 15, 19, 30, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 4, 15, 18, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 7,
                columns: new[] { "End", "Start" },
                values: new object[] { new DateTime(2020, 12, 24, 23, 58, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 12, 24, 22, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 8,
                columns: new[] { "AllDay", "End", "Start" },
                values: new object[] { true, new DateTime(2021, 1, 19, 23, 59, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 1, 18, 23, 59, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllDay",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "End",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "Start",
                table: "Events");

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Events",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 2, 14, 17, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 3, 27, 23, 51, 15, 0, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 3, 30, 18, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 4, 4, 20, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 4, 15, 12, 30, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 4, 15, 18, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 7,
                column: "Date",
                value: new DateTime(2020, 12, 24, 23, 58, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 8,
                column: "Date",
                value: new DateTime(2021, 1, 18, 23, 59, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
