﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DIPSCalendar.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CalendarDates",
                columns: table => new
                {
                    Year = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Day = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarDates", x => new { x.Year, x.Month, x.Day });
                });

            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    EventID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EventDesc = table.Column<string>(nullable: true),
                    DateYear = table.Column<int>(nullable: true),
                    DateMonth = table.Column<int>(nullable: true),
                    DateDay = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.EventID);
                    table.ForeignKey(
                        name: "FK_Events_CalendarDates_DateYear_DateMonth_DateDay",
                        columns: x => new { x.DateYear, x.DateMonth, x.DateDay },
                        principalTable: "CalendarDates",
                        principalColumns: new[] { "Year", "Month", "Day" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Events_DateYear_DateMonth_DateDay",
                table: "Events",
                columns: new[] { "DateYear", "DateMonth", "DateDay" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropTable(
                name: "CalendarDates");
        }
    }
}
