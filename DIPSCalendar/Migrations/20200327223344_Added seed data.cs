﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DIPSCalendar.Migrations
{
    public partial class Addedseeddata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 3, 27, 23, 33, 43, 682, DateTimeKind.Local).AddTicks(5553));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "EventID",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 3, 27, 23, 27, 43, 583, DateTimeKind.Local).AddTicks(6871));
        }
    }
}
