﻿
class EventModel {
  constructor(props) {
    this.eventID = props.eventID || 0;
    this.eventDesc = props.eventDesc || "";
    this.title = this.eventDesc;
    this.start = new Date(props.start);
    this.end = new Date(props.end);
    this.allDay = props.allDay || false;
  }
}

export default EventModel;