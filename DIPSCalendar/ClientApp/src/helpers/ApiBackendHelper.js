﻿
class ApiBackendHelper {
  constructor() {
    this.baseUrl = "/api/event";
  }

  async getEvents() {
    try {
      let response = await fetch(this.baseUrl);
      if (response.status === 200) {
        let data = await response.json();
        return data;
      }
      return { error: response.statusText };

    } catch (e) {
      return { error: e };
    }
  }

  async makeEvent(event) {
    try {
      let response = await fetch(this.baseUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(event)
      });
      
      if (response.status === 201) {
        let data = response.json();
        return data;
      }
      return { error: response.statusText }; 

    } catch (e) {
      return { error: e };
    }
  }

  async deleteEvent(eventID) {
    let url = this.baseUrl + "/" + eventID;
    try {
      return await fetch(url, { method: 'DELETE' })
        .then((response) => response.json())
        .then((data) => { return data; });
    } catch (e) {
      return { error: e };
    }
  }

}

export default new ApiBackendHelper();