﻿import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { Calendar, Views, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import ApiBackendHelper from '../helpers/ApiBackendHelper';
import EventModel from '../helpers/EventModel';
import 'moment/locale/nb';
import 'react-big-calendar/lib/css/react-big-calendar.css';

export class MyCalendar extends Component {

  constructor() {
    super();
    this.state = {
      localizer: momentLocalizer(moment),
      messages: {
        allDay: 'Hele dagen',
        previous: 'Forrige',
        next: 'Neste',
        today: 'I dag',
        month: 'Måned',
        week: 'Uke',
        day: 'Dag',
        agenda: 'Agenda',
        date: 'Dato',
        time: 'Tid',
        event: 'Hendelse',
      },
      eventsList: [],
      isLoading: true,
      error: ""
    }
    this.handleSelect = this.handleSelect.bind(this);
    this.handleSelectEvent = this.handleSelectEvent.bind(this);
  }

  componentDidMount() {
    this.populateEventsList();
  }


  async populateEventsList() {
    let response = await ApiBackendHelper.getEvents();
    
    let list = [];
    for (let i = 0; i < response.length; i++) {
      list[i] = new EventModel(response[i]);
    }

    if (response.error) {
      this.setState({
        isLoading: false,
        error: response.error
      })
    } else {
      this.setState({
        isLoading: false,
        eventsList: list
      });
    }
  }

  async handleSelect({ start, end }) {
    let title = window.prompt('Legg til hendelse');
    let allDay = false; 
    if (end.getHours() - start.getHours() >= 24) allDay = true;

    if (title) {
      let newEvent = new EventModel({ eventDesc: title, start: start, end: end, allDay: allDay });
      let response = await ApiBackendHelper.makeEvent(newEvent);
      
      this.setState(prevState => ({
        eventsList: [...prevState.eventsList, newEvent],
        error: response.error
      }));
    }
  }

  handleSelectEvent(event) {
    let start = event.start;
    let startString = start.getDate() + "." + start.getMonth() +" "+ start.getHours() +":"+ start.getMinutes();
    let end = event.end;
    let endString = end.getDate() + "." + end.getMonth() +" "+ end.getHours() + ":" + end.getMinutes();


    let confirmString = "Start: " + startString + "\nEnd: " + endString + "\n" + event.title + "\n\nDelete event?";
    let deleteEvent = window.confirm(confirmString);

    if (deleteEvent) {
      let response = ApiBackendHelper.deleteEvent(event.eventID);

      let filtered = this.state.eventsList.filter(e => e.eventID !== event.eventID);
      this.setState({
        eventsList: filtered,
        error: response.error
      })
    }
  }


  render() {

    if (this.state.isLoading) {
      return <Container>Laster inn...</Container>;
    } else if (this.state.error) {
      return <Container>{this.state.error}</Container>;
    }

    return (
      <Container>
        <Calendar
          selectable={true}
          localizer={this.state.localizer}
          events={this.state.eventsList}
          defaultView={Views.MONTH}
          messages={this.state.messages}
          style={{ height: 600 }}
          onSelectEvent={this.handleSelectEvent}
          onSelectSlot={this.handleSelect}
        />
      </Container>   
    );
  }
}

