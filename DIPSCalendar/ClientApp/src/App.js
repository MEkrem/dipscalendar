import React, { Component } from 'react';
import { NavMenu } from './components/NavMenu';
import { MyCalendar } from './components/MyCalendar';
import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render() {

    return (
      <div>
        <NavMenu />
        <MyCalendar />
      </div> 
    );
  }
}
